import numpy as np
import sklearn
from sklearn import linear_model
import matplotlib as mt
from matplotlib import pyplot as plt
clf=linear_model.LinearRegression()
a=0
a1=1
b=1
i=0
K=1
n=0
m=1
p=1
A=np.loadtxt("sunspot traid1.txt")#Import traid data
#A=np.array([1,2,3,4,5,6,7,8,9])
#A=[y for x in A for y in x]
print len(A)
print A
print "len traid"
A1=[]
A2=[]
A4=[]
n1=len(A)-1
n2=len(A)
x=1
i1=0
m1=0
m2=0
U=[]
U1=[]
U3=[]
l=0
L=[]
L1=[]
I=1
while K<len(A):#Depth of immersion traid
    while i<len(A)-m:#Sampling cycle for x fact and y fact
        c = A[a:b]#x fact
        c=np.append(c,I)
        #print c
        c1=A[a1:n2]#y fact
        b=b+1
        a=a+1
        A1.append(c)#array for x fact
        i=i+1
    #print A1
    #I=1
    #A1.append(I)
    #print A1
    K=K+1
    b=K
    i=0
    a=0
    a1=a1+1
    m=m+1
    A3=A1[n:]#array for x fact on step K
    #print A3
    #print c1
    clf.fit (A3,c1)# w
    n=n+(len(A)-x)
    x=x+1
    n1=n1*2-1
    A4.append(clf.coef_)#array for w
    B=np.array(A4)# numpy array for w
#print B
D=np.loadtxt("sunspot valid1.txt")
print len(D)
#D=[y for x in D for y in x]
print "lenD"
K1=0
nv=1
mv=1
bv=1
av=0
p=len(D)-1
iv=0
ov=1
av1=1
nv1=2
D1=[]
D2=[]
u=0
u2=len(D)-1
mv1=0
mv2=0
while K1<len(D)-1:#Depth of immersion valid
    del D1[:]
    del U3[:]
    while iv<p:#Sampling cycle for x fact and y fact
        cv=D[av:bv]
        cv=np.append(cv,I)
        cvy=D[av1:nv1]
        av=av+nv
        bv=bv+mv
        av1=av1+1
        nv1=nv1+1
        iv=iv+1
        D1.append(cv)#array for x fact
        U3.append(cvy)# array for y fact
    yp=D1*B[K1]#cycle for x*w
    #print B[K1]
    del U[:]
    while u<u2:#cycle for sum x*w...for predict y...yp  
        sumDelta=sum(yp[u])
        u=u+1
        U.append(sumDelta)#array for y predict
    U4=[y for x in U3 for y in x]#formatting y fact
    del L[:]
    while l<len(U4):#cycle for target error
        l1=((U[l]-U3[l])**2)/(2*len(D))#target error valid
        L.append(l1)
        l=l+1
    L1.append(sum(L))#all array target error valid
    K1=K1+1
    l=0
    bv=ov+1
    av1=K1+1
    av=0
    mv=1
    mv1=1
    mv2=1
    nv1=3
    nv=1
    av1=2
    p=len(D)-K1-1
    u2=len(D)-K1-1
    ov=ov+1
    iv=0
    u=0
print min(L1)#min target error valid
print "min valid target error"
C=L1.index(min(L1))#number of min target error valid
print C+1
print "K"
print B[C]
print "W"
plt.plot(L1)#plot of target error valid and K
plt.show()
E=np.loadtxt("sunspot contr1.txt")#control sample
print len(E)
#E=[y for x in E for y in x]
print "lenE"
K3=len(B[C])
K4=K3+1
k3=0
E5=[]
while K3<len(E):
    E1=E[k3:K3]*B[C]#W*x fact
    #print E1
    Controlsum=sum(E1)#y predict control
    #print Controlsum
    E2=E[K3:K4]
    E4=E2-Controlsum
    E5.append(E4)
    #print E5
    K3=K3+1
    k3=k3+1
    K4=K4+1
Cc=((sum(E5)**2))/(2*len(E))#target error control
print Cc
print "target error control"
print (Cc/min(L1))*100
print "%"
#print E[0:31]
#Um=sum(E[0:31]*B[C])
#print Um
#print "um"
#print E[0:32]-U
